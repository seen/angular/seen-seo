# Seen SEO

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-seo/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-seo/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b9df6e2d4ad94e0bb9e035bd29d4cfcd)](https://www.codacy.com/app/seen/seen-seo?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-seo&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-seo/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-seo/commits/develop)

